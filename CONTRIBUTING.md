Bijdragen
=========

Je eigen recept (of andere verbetering) bijdragen is welkom.
Het proces is als volgt:

 1. Maak een *merge request* met je wijzigingen.
 1. Zorg dat de *CI* groen geeft.
    Als je vragen hebt over de meldingen kun je die stellen in
    de comments bij het merge request.
 1. Verwerk de feedback op je merge request.
 1. Iemand van het project *merged* je merge request,
    je wijzigingen worden snel zichtbaar op de site.

Nieuw recept
------------

Een nieuw recept aanmaken gaat met Hugo:

```sh
hugo new <categorie>/<naam>.md
```

Gebruik in de `naam` een `-` in plaats van een spatie.
Bewerk vervolgens het bestand dat Hugo aan heeft gemaakt,
en vul daar de gegevens over je recept in.
