---
title: Siciliaanse pastasaus
subtible: Salsa al Siciliana
bannerImage: salsa_al_siciliana.jpg

tags:
  - pasta
  - saus
  - vegetarisch

yield: 2 personen
workTime: 20 min
totalTime: 25 min
ingredients:
  - 1 aubergine
  - 1 rode cayennepeper
  - 250 g groene olijven
  - 500 g gezeefde tomaten
  - 2 knoflooktenen
  - 1 el kappertjes
  - 15 g verse basilicum
  - 100 g parmezaanse kaas
  - 200 g tagliatelle
  - Olijfolie
  - Zout
  - Oregano
---

Dit recept komt van een [enthousiaste Italiaan en Fransman op YouTube][1].
Een geweldig heerlijke pasta volgens Italiaans recept, simpel en vegetarisch.

  1. Snij de aubergine in blokjes van 2 tot 3 cm en bak deze in een koekenpan met olijfolie.
  1. Snij de knoflook en rode peper in plakjes terwijl de aubergine rustig aan bakt.
  1. Fruit de knoflook en rode peper in een tweede pan met olijfolie.
  1. Halveer de olijven en voeg deze toe aan de tweede pan.
  1. Mik ook de kappertjes in de tweede pan.
  1. Voeg de tomatensaus toe aan de tweede pan.
  1. Snijd de Basilicum en meng deze samen met de eetlepel oregano en zout toe aan de tomatensaus.
  1. Laat de saus op middellaag vuur pruttelen met een deksel.
  1. Je kunt nu de tagliatelle koken volgens instructie. Dit kan ook even wachten, aangezien de saus lekkerder word als hij nog even staat.
  1. Wanneer de aubergine klaar is kan je deze alvast uitzetten.
  1. Combineer de pasta met de aubergine en tomatensaus zodra de pasta gaar is.

Ganeer het recept met een de parmasaanse kaas en eventueel een blaadjebasilicum.

## Variatie

Voor de variatie kan je nog altijd proberen om meer basilicum, knoflook of rode peper toe te voegen, of verse (zelfgemaakte) pasta te gebruiken.

[1]: https://www.youtube.com/watch?v=zqPe_3iPdgg
