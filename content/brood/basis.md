---
title: "Brood: wit, bruin of volkoren"
date: 2020-04-27T13:57:20+02:00

tags:
  - brood
  - witbrood
  - bruinbrood
  - volkorenbrood
  - oven

yield: 1 brood
workTime: 20 min
totalTime: 3 uur
ingredients:
  - 500 g bloem of meel
  - 10 g (1 tl) zout
  - 7 g (2 tl) gist
  - 320-340 ml lauwwarm water
  - "Bij volkoren: 10 g boter"
  - "Bij volkoren: 4 g (1 tl) suiker"

---
Het recept voor witbrood, bruinbrood en volkorenbrood is vrijwel identiek,
en voornamelijk afhankelijk van de verhouding.
De totale hoeveelheid water is afhankelijk van de hoeveelheid (volkoren)meel:
320 ml voor een witbrood, en 340 ml voor volkoren.

Een volkorenbrood is van zichzelf wat bitter,
dit is tegen te gaan met boter en suiker.
De hoeveelheden zijn een indicatie, eigen voorkeur is leidend.

Instructies voor het bakken zijn als volgt:

 1. Mix het bloem en zout, en (eventueel) boter en suiker, in een kom.
 1. Maak een kuiltje, en gooi daar het water in.
 1. Activeer het gist in het water, en mix vervolgens alles in de kom.
 1. Laat het deeg een uur rijzen op een warme plek tot het in volume verdubbeld is.
 1. Vouw het deeg in de gewenste vorm en leg het op een bakplaat, of in een bakvorm.
 1. Laat het deeg weer een uur op een warme plek rijzen.
 1. Verwarm de oven voor op 240°C.
 1. Bak het brood in ongeveer 35 minuten goudbruin.
