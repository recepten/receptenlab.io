---
title: Napoletana

tags:
  - pizza
  - napoletana
  - oven

yield: 6 pizza's
workTime: 20 min
totalTime: 9 tot 25 uur
ingredients:
  - 500 ml lauwwarm water
  - 825-900 g (patent)bloem
  - 0,5 g (1/4 tl) instant gist of 1,5 g verse gist
  - 25 g zout
---

Dit recept is gebaseerd op het [officiële recept][1] van de
*Associazione Verace Pizza Napoletana*.
Volg hun instructies als je het écht goed wil doen.

Als je echter geen oven hebt die tot 430-480°C gaat,
is er ook een thuismethode met een pan en de grill.

1. Meng 825 g bloem en het zout in een kom of op het aanrecht.
1. Maak hierin een uitsparing voor het water.
1. Meng het gist door het water.
1. Kneed het deeg zo goed mogelijk.
   Voeg extra bloem toe als het deeg te plakkerig blijft.
1. Laat het deeg 1 uur rusten.
1. Verdeel het deeg in 6 balletjes.
1. Laat het deeg 7 uur tot 23 uur afgedekt rijzen afhankelijk van de temperatuur.
1. Verwarm de oven voor op minimaal 280°C, bij voorkeur met de (boven)grillstand.
1. Vorm de bolletjes met de hand, vanuit het midden, tot pizzabodem.
   Dit kan (met wat olijfolie) in de pan.
1. Bedek de pizza met saus en lekkers.

Voor het bakken zijn er twee alternatieven op een steenoven:

1. Op een pizzastaal in 3-5 minuten.
1. Met de pan en de grill:
   1. Bak de pizza 2,5 minuten op hoog vuur,
      of tot de onderkant bruin begint te worden.
   1. Bak de pizza 2,5-5 minuten in de oven,
      of tot de bovenkant begint te verbranden.
      Dit kan op een bakplaat of in de pan als deze daar geschikt voor is.

[1]: https://www.pizzanapoletana.org/en/ricetta_pizza_napoletana
