---
title: "Panpizza"
date: 2020-04-27T09:49:25+02:00
draft: false

tags:
- italiaans
- pizza
- oven

yield: 2 pizza's
workTime: 15 min
totalTime: 4 tot 26 uur
ingredients:
- Een ovenbestendige pan (280°C)
- 400g bloem
- 10g (2 tl) zout
- 4g (1 tl) gist
- 275g lauwwarm water
- 8g (2tl) olijfolie
---

 1. Mix het bloem en zout in een kom.
 1. Maak een kuiltje, en gooi daar het water en de olijfolie in.
 1. Activeer het gist in het water, en mix vervolgens alles in de kom.
 1. Twee opties:
    1. kneed het geheel tot een glad deeg en laat het 2 uur rijzen, of
    1. laat het een nachtje staan (8-24 uur).
 1. Verdeel het deeg in twee ballen.
 1. Vet pannen in met 1-2 tl olijfolie.
 1. Plaats het deeg in de pan en rol het door de olie,
    druk het vervolgens een beetje plat.
 1. Laat het deeg 2 uur afgedekt rijzen.
 1. Verwarm de oven voor op 290°C.
 1. Bedek je pizza met saus en lekkers.
 1. Bak de pizza in 12 tot 15 minuten goudbruin.
