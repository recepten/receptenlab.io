---
title: Thuisgemaakt

tags:
  - pizza
  - oven

yield: 2 grote pizza's
workTime: 20 min
totalTime: 1,5 uur
ingredients:
  - 325 ml lauwwarm water
  - 500 g (patent)bloem
  - 4 g (1 tl) instant gist
  - 5 g zout
---

Dit recept komt van een [enthousiaste italiaan op YouTube][1].
Het heeft als grote voordeel dat het significant sneller is dan [Pizza Napoletana](/pizza/napoletana).
Dit recept beschrijft alleen het deeg en het bakken,
bekijk [de video op YouTube][1] voor de aangeraden topping.

1. Meng het bloem en het zout.
1. Activeer het gist in een kopje van het water.
1. Meng de rest van het water door het bloem.
1. Voeg het mengsel van gist en water toe.
1. Kneed het deeg voor 10 minuten.
1. Laat het deeg 30 minuten afgedekt rijzen tot het in volume verdubbeld is.
1. Verdeel het deeg in 2 balletjes.
1. Laat het deeg wederom 30 minuten afgedekt rijzen.
1. Verwarm de oven voor op de hoogste temperatuur mogelijk, minimaal 220°C.
1. Vorm de bolletjes met de hand, vanuit het midden, tot pizzabodem.
1. Bedek de pizza's met saus en lekkers.
1. Bak de pizza's 8-10 minuten.

[1]: https://www.youtube.com/watch?v=n-VRntrbypI
