---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true

tags:
  - ...

yield: 1 portie
workTime: 15 min
totalTime: 1 uur
ingredients:
  - ...
---

